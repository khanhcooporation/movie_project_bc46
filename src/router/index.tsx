import { RouteObject } from 'react-router-dom'
import { AuthLayout, MainLayout } from 'components/layouts'
import { Home, Login, Register } from 'page'
import { PATH } from 'constant'
import { Demo } from 'demo'
import Account from 'page/Account'

console.log('PATH: ', PATH)
export const router: RouteObject[] = [
    {
        path: '/demo',
        element: <Demo />,
    },
    {
        path: '/',
        element: <Home />,
    },
{
    path:'/',
    element: <MainLayout/>,
    children: [
        {
            index: true,
            element: <Home />,
        },
        {
            path: PATH.account,
            element: <Account />
        }
    ],
},

    {
        element: <AuthLayout />,
        children: [
            {
                path: PATH.login,
                element: <Login />,
            },
            {
                path: PATH.register,
                element: <Register />,
            },
        ],
    },
]
