import { zodResolver } from "@hookform/resolvers/zod";
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";
import { SubmitHandler, useForm } from "react-hook-form";
import { LoginSchema, LoginSchemaType } from "schema/LoginSchema";
// import { quanLyNguoiDungServices } from "services";
import { toast } from "react-toastify";
import { useAppDispatch } from "store";
import { loginThunk } from "store/quanLyNguoiDung/thunk";
import { Input } from "components/ui";

const LoginTemplate = () => {
  const navigate = useNavigate();
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm<LoginSchemaType>({
    mode: "onChange",
    resolver: zodResolver(LoginSchema),
  });

  const dispatch = useAppDispatch();
  // useDispatch() vẫn sẽ xài được nhưng sẽ bị báo lỗi TypeScript
  //?Để khắc phục: phải sd hàm useAppDispatch tự định nghĩa kiểu dữ liệu tự config trên store

  const onSubmit: SubmitHandler<LoginSchemaType> = async (value) => {
    // try {
    //   await quanLyNguoiDungServices.login(value)
    //   toast.success("Đăng nhập thành công");
    //   navigate("/");
    // } catch (error) {
    //   console.log(error);
    //   toast.error(error?.response?.data?.content);
    // }
    dispatch(loginThunk(value))
            .unwrap()
            .then(() => {
                toast.success('Đăng nhập thành công!')
                navigate('/')
            })
  };
  return (
    <form className="pt-[30px] py-[40px]" onSubmit={handleSubmit(onSubmit)}>
      <h1 className="text-white text-40 font-600">Sign In</h1>
      <div className="mt-40">
        <input
          type="text"
          placeholder="Tài Khoản"
          className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          {...register("taikhoan")}
        />
        <p className="text-red-500">{errors?.taikhoan?.message}</p>
        {/* <Input register={register} name="taiKhoan" error={errors?.taikhoan?.message}  placeholder="Tài Khoản" /> */}
      </div>
      <div className="mt-40">
        {/* <input
          type="password"
          placeholder="passWord"
          className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
          {...register("matKhau")}
        />
        <p className="text-red-500">{errors?.matKhau?.message}</p> */}
        <Input register={register} name="matKhau" error={errors?.matKhau?.message} placeholder="Mật Khẩu" type="password" />
      </div>
      <div className="mt-40">
        <button className="text-white bg-red-500 hover:bg-red-800 font-medium rounded-full text-sm text-center mr-2 mb-2 transition-all ease-in-out px-5 py-[16px] text-20 w-full">
          Sign In
        </button>
      </div>
      <p className="mt-10 text-white">
        Chưa có tài khoản?
        <span
          className="text-blue-500 cursor-pointer"
          onClick={() => {
            navigate(PATH.register);
          }}
        >
          Đăng ký?
        </span>
      </p>
    </form>
  );
};

export default LoginTemplate;
