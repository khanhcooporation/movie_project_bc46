import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from 'react-hook-form'
import { RegisterSchema, RegisterSchemaType } from "schema";
import { quanLyNguoiDungServices } from "services";
import { toast } from 'react-toastify'
import { useNavigate } from "react-router-dom";
import { PATH } from "constant";

const RegisterTemplate = () => {
  const {
    handleSubmit,
    register,
    formState: { errors },
} = useForm<RegisterSchemaType>({
    mode: 'onChange',
    resolver: zodResolver(RegisterSchema),
});
  
console.log("err: ", errors);

const navigate = useNavigate();

const onSubmit: SubmitHandler <RegisterSchemaType> = async(value) =>{
  try {
    console.log("value: ", {value});
    await quanLyNguoiDungServices.register(value);
    toast.success("Đăng ký thành công!")
    navigate(PATH.login)
  } catch (error) {
    console.log(error);
    toast.error(error?.response?.data?.content);
  }
  
  
}

  return (
    <form className="pt-[30px] py-[40px]" onSubmit={handleSubmit(onSubmit)}>
      <h1 className="text-white text-40 font-600">Register</h1>
      <div className="mt-20">
        <input type="text" placeholder="Tài khoản" className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500"
        //   {...register('
        //    {
        //     required: 'Vui lòng nhập tài khoản',
        //     minLength: {
        //         value: 6,
        //         message: 'Nhập từ 6 ký tự',
        //     },
        //     maxLength: {
        //         value: 20,
        //         message: 'Nhập tối đa 20 ký tự',
        //     },
        //     pattern: {
        //       // value: ,
        //       message: 'abc'
        //     }
        // }
        // )}
        {...register("taiKhoan")}
         />
        <p className="text-red-500">{errors?.taiKhoan?.message as string}</p>
      </div>
      <div className="mt-20">
        <input type="password" placeholder="Mật khẩu" className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500" {...register("matKhau")}/>
        <p className="text-red-500"> {errors?.taiKhoan?.message}</p>
      </div>
      <div className="mt-20">
        <input type="text" placeholder="Email" className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500" {...register("email")}/>
        <p className="text-red-500"> {errors?.email?.message}</p>

      </div>
      
      <div className="mt-20">
        <input type="text" placeholder="Số điện thoại" className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500" {...register("soDt")}/>
        <p className="text-red-500"> {errors?.soDt?.message}</p>

      </div>
      <div className="mt-20">
        <input type="text" placeholder="Mã Nhóm" className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500" {...register("maNhom")}/>
        <p className="text-red-500"> {errors?.maNhom?.message}</p>
      </div>
      <div className="mt-20">
        <input type="text" placeholder="Họ Và Tên" className="outline-none block w-full p-10 text-white border border-white-300 rounded-lg bg-[#333] focus:ring-blue-500 focus:border-blue-500" {...register("hoTen")}/>
        <p className="text-red-500"> {errors?.hoTen?.message}</p>
      </div>
      <div className="mt-20">
        <button  className="text-white bg-red-500 hover:bg-red-800 font-medium rounded-full text-sm text-center mr-2 mb-2 transition-all ease-in-out px-5 py-[16px] text-20 w-full">Sign In</button>
      </div>
    </form>
  )
}

export default RegisterTemplate;