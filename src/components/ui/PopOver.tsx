import {Popover as PopoverA, PopoverProps as PopoverPropsA} from 'antd'

export const PopOver = (props : PopoverPropsA) => {
  return (
    <PopoverA {...props} />
    )
}

export default PopOver