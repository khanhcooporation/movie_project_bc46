import { NavLink, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Avatar, PopOver } from ".";
import { UserOutlined } from "@ant-design/icons";
import { PATH } from "constant";
import { useAuth } from "hooks";
import { useDispatch } from "react-redux";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";

export const Header = () => {
  const navigate = useNavigate();
  const { user } = useAuth();
  const dispatch = useDispatch();
  return (
    <HeaderS>
      <div className="header-content">
        <h2 className="font-600 text-[30px]">CYBER MOVIE</h2>
        <div className="flex justify-around items-center">
          <NavLink className="mr-40" to="">
            About
          </NavLink>
          <NavLink to="">Contact</NavLink>
          {user && (
            <PopOver
              content={
                <div className="p-10">
                  <h2 className="p-10 font-600">{user?.hoTen}</h2>
                  <hr />
                  <div
                    className="p-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300 rounded-lg"
                    onClick={() => {
                      navigate(PATH.account);
                    }}
                  >
                    Thông Tin Tài Khoản
                  </div>
                  <div
                    className="p-10 cursor-pointer hover:bg-gray-500 hover:text-white transition-all duration-300 rounded-lg"
                    onClick={() => {
                      dispatch(quanLyNguoiDungActions.logOut());
                    }}
                  >
                    Đăng Xuất
                  </div>
                </div>
              }
              trigger="click"
            >
              <Avatar
                className="!ml-40 cursor-pointer !flex !justify-center !items-center"
                size={28}
                icon={<UserOutlined />}
              />
            </PopOver>
          )}
          {!user && (
            <p
              className="font-600 text-16 ml-10"
              onClick={() => {
                navigate(PATH.login);
              }}
            >
              Login
            </p>
          )}
        </div>
      </div>
    </HeaderS>
  );
};

export default Header;

const HeaderS = styled.header`
  height: var(--header-height);
  background: white;
  box-shadow: 0 0 5px rgba(1, 1, 1, 0.4);
  .header-content {
    max-width: var(--max-width);
    margin: auto;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 100%;
  }
`;
