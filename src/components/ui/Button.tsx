import { Button as ButtonA, ButtonProps as ButtonPropsA } from "antd"

type ButtonProps = ButtonPropsA & {
    //định nghĩa các props có thể truyền xuống

}
export const Button = (props: ButtonProps) => {
    return <ButtonA {...props} />
}


//? GOM BUTTON ra để tiện sửa hàng loạt