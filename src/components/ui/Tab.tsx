import {Tabs as TabsA, TabsProps as TabsPropsA} from 'antd'

type TabsProps= TabsPropsA & {
    //props
}

export const TabA = (props: TabsProps) => {
  return (
    <TabsA {...props} />
  )
}
