import { z } from "zod";

export const AccountSchema = z.object({
    taiKhoan: z.string().nonempty("Vui lòng nhập tài khoản").max(20,"Tài khoản tối đa 20 ký tự").min(6, "Tài khoản tối thiểu 6 ký tự"), //Kiểu dữ liệu phụ thuộc vào BE
    email: z.string().nonempty("Vui lòng nhập Email").email("Vui Lòng nhập đúng Email"),
    soDt: z.string().nonempty('Vui lòng nhập số điện thoại'),
    maNhom: z.string().nonempty('Vui lòng nhập mã nhóm'),
    hoTen: z.string().nonempty('Vui lòng nhập họ tên'),
    maLoaiNguoiDung: z.string().nonempty('Vui lòng nhập mã loại người dùng'),
})

export type AccountSchemaType = z.infer<typeof AccountSchema>