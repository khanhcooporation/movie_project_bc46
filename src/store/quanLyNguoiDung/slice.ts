import { createSlice } from "@reduxjs/toolkit";
import { getUserThunk, loginThunk, updateUserThunk } from "./thunk";
import { User, UserInfo } from "types";
type QuanLyNguoiDungInitialState = {
  user?: UserInfo | User;
  accessToken?: string;
  isUpdatingUser?: boolean;
};

const initialState: QuanLyNguoiDungInitialState = {
  accessToken: localStorage.getItem("accessToken"),
  isUpdatingUser: false,
};
const quanLyNguoiDungSlice = createSlice({
  name: "quanLyNguoiDung",
  initialState,
  reducers: {
    logOut: (state) => {
      state.user = undefined;
      localStorage.removeItem("accessToken");
    },
  }, // Xử lý action đồng bộ
  extraReducers: (builder) => {
    //1 promise: mending reject fullfilled(successs)
    builder
      .addCase(loginThunk.fulfilled, (state, { payload }) => {
        console.log("FULLFILL payload: ", payload);
        state.user = payload;
        //Lưu thông tin đăng nhập vào LocalStorage
        if (payload) {
          localStorage.setItem("accessToken", payload.accessToken); //* ko cần JSON.stringify(payload.accessToken) vì nó là string
        }
      })
      .addCase(getUserThunk.fulfilled, (state, { payload }) => {
        if (payload) {
          state.user = payload;
        }
      })
      .addCase(updateUserThunk.pending, (state) => {
        state.isUpdatingUser = true;
      })
      .addCase(updateUserThunk.fulfilled, (state) => {
        state.isUpdatingUser = false;
      })
      .addCase(updateUserThunk.rejected, (state) => {
        state.isUpdatingUser = false;
      });
    // .addCase(loginThunk.pending, (state, {payload}) =>{
    //   console.log("PENDING payload: ",payload)
    // })

    // .addCase(loginThunk.rejected, (state, {payload}) =>{

    //   console.log('REJECT payload: ', payload);
    // });

    //xử lý action bất đồng bộ (CALL API)
  },
  //MiddleWare:..
  //redux thunk( có sẵn trong toolkit ) vs redux saga
});

export const {
  reducer: quanLyNguoiDungReducer,
  actions: quanLyNguoiDungActions,
} = quanLyNguoiDungSlice;
