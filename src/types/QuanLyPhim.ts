export interface Movie {
    maPhim: number
    tenPhim: string
    biDanh: string
    trailer: string
    hinhAnh: string
    moTa: string
    maNhom: string
    ngayKhoiChieu: string
    danhGia: number
    hot: boolean
    dangChieu: boolean
    sapChieu: boolean
}

//! Coi lại cách xài tool trên mạng (9:10)