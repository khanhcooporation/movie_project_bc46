export type User ={
    taiKhoan: "string",
    hoTen: "string",
    email: "string@email.com",
    soDt: "01235421",
    maNhom: "GP00",
    maLoaiNguoiDung: "KhachHang",
    accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoic3RyaW5nIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvZW1haWxhZGRyZXNzIjoic3RyaW5nQGVtYWlsLmNvbSIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6WyJLaGFjaEhhbmciLCJzdHJpbmdAZW1haWwuY29tIiwiR1AwMCJdLCJuYmYiOjE2OTI3MDc2NTgsImV4cCI6MTY5MjcxMTI1OH0.9n6GZ56Q-1-kold1mNigm-jd9PhZ3uOe-_vV3LsMcUU"
 
}

export type UserInfo = User & {
    loaiNguoiDung: {
        maLoaiNguoiDung: string
        tenLoai: string
    }
    thongTinDatVe: []
}