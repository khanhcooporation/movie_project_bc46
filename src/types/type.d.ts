declare type ApiResponse<T> = {
    statusCode:number
    message: string
    content: T  //generics (phụ thuộc vào biến truyền vào)
}