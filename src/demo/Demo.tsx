
export const Demo = () => {
  return (
    <div>
        <h2 className="text-2xl">Demo</h2>
        <p className="text-lg text-16 font-800 p-12 text-[#333] py-[20px] border border-red-950 hover:text-red-500 transition-all ease-in-out duration-300">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dolore, maiores!</p>
    </div>
  )
}

export default Demo