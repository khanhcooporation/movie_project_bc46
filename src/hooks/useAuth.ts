//Đặt có từ khóa "use" đầu tiên (React sẽ ngầm định là hook tự định nghĩa)
//Có sự khác biệt: ko thẻ sử dụng HOOK ở hàm bth

import { useSelector } from "react-redux"
import { RootState } from "store"

//!                Nhưng lại có thể ở hàm HOOK tự tạo
//Lấy thông tin user đăng nhập
export const useAuth =() =>{
    const {user} = useSelector( (state:RootState) => state.quanLyNguoiDung )
    return { user }
}