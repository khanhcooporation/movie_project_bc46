import RegisterTemplate from "../components/templates/RegisterTemplate"

export const Register = () => {
  return (
    <div><RegisterTemplate/></div>
  )
}

export default Register